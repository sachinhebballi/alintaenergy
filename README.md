# Alinta Energy Coding Test

The application is developed in ASP.NET Core 2.2 and the tests are developed using XUnit and Fluent Assertions.

--------------------------------------

Steps to run the application:

If you are running it on Visual Studio.

Press F5 or Select Debug -> Start without Debugging

Steps to Run Tests

Open the Test Explorer from the View Menu.

Click on Run All

----------------------------------------

If you are running it on VS Code

Open the terminal and use the following commands:

dotnet build

dotnet serve

Commands to Run Tests

dotnet test