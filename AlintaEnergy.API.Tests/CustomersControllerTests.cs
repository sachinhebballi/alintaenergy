﻿using System.Threading.Tasks;
using AlintaEnergy.API.Controllers;
using AlintaEnergy.API.Models;
using AlintaEnergy.API.Providers;
using AlintaEnergy.API.Services;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace AlintaEnergy.API.Tests
{
    public class CustomersControllerTests
    {
        private readonly Mock<ICustomerService> _customerService;

        public CustomersControllerTests()
        {
            _customerService = new Mock<ICustomerService>();
        }

        [Fact]
        public async void ShouldSuccessfullyAddACustomer()
        {
            var customer = new CustomerModel
            {
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            _customerService.Setup(_ => _.AddCustomer(customer)).Returns(Task.FromResult(true));

            var controller = new CustomersController(_customerService.Object);
            var result = await controller.AddCustomer(customer) as StatusCodeResult;

            result.Should().NotBe(null);
            result.StatusCode.Should().Be(201);
        }

        [Fact]
        public async void ShouldReturnBadRequestForInvalidCustomer()
        {
            CustomerModel customer = null;

            var controller = new CustomersController(_customerService.Object);
            var result = await controller.AddCustomer(customer) as BadRequestObjectResult;

            result.Should().NotBe(null);
            result.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void ShouldSuccessfullyUpdateCustomer()
        {
            var customer = new CustomerModel
            {
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            _customerService.Setup(_ => _.UpdateCustomer(customer)).Returns(Task.FromResult(true));

            var controller = new CustomersController(_customerService.Object);
            var result = await controller.UpdateCustomer(customer) as OkResult;

            result.Should().NotBe(null);
            result.StatusCode.Should().Be(200);
        }

        [Fact]
        public async void ShouldReturnBadRequestForEmptyCustomer()
        {
            CustomerModel customer = null;

            var controller = new CustomersController(_customerService.Object);
            var result = await controller.UpdateCustomer(customer) as BadRequestObjectResult;

            result.Should().NotBe(null);
            result.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void ShouldSuccessfullyDeleteCustomer()
        {
            var customerId = "645C234F-C8E4-4A4A-B579-F4A8A93CA03C";

            _customerService.Setup(_ => _.DeleteCustomer(customerId)).Returns(Task.FromResult(true));

            var controller = new CustomersController(_customerService.Object);
            var result = await controller.DeleteCustomer(customerId) as OkResult;

            result.Should().NotBe(null);
            result.StatusCode.Should().Be(200);
        }
    }
}
