﻿using AlintaEnergy.API.Models;
using AlintaEnergy.API.Providers;
using AlintaEnergy.API.Providers.Entities;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AlintaEnergy.API.Tests
{
    public class CustomerProviderTests
    {
        private readonly DbContextOptions<CustomersContext> _options;

        private readonly CustomersContext _context;

        public CustomerProviderTests()
        {
            _options = new DbContextOptionsBuilder<CustomersContext>().UseInMemoryDatabase(databaseName: "Customers").Options;
            _context = new CustomersContext(_options);
            var customer = new Customer
            {
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            _context.Customers.Add(customer);
            _context.SaveChanges();
        }

        [Fact]
        public async Task ShouldSuccessfullyAddCustomers()
        {
            var customer = new CustomerModel
            {
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            var customerProvider = new CustomerProvider(_context);
            await customerProvider.AddCustomer(customer);

            _context.Customers.FirstOrDefault(_ => _.FirstName == "John").Should().NotBe(null);
        }

        [Fact]
        public async Task ShouldSuccessfullyReturnCustomersOnSearch()
        {
            var customerFirstName = "Joh";

            var customerProvider = new CustomerProvider(_context);
            var customersSearchResult = await customerProvider.SearchCustomers(customerFirstName);

            customersSearchResult.Should().NotBeNull();
            customersSearchResult.Should().AllBeOfType<CustomerModel>();
        }

        [Fact]
        public async Task ShouldSuccessfullyUpdateCustomer()
        {
            var updatedCustomer = _context.Customers.Select(_ => new CustomerModel{ CustomerId = _.CustomerId, FirstName = _.FirstName, LastName = _.LastName}).FirstOrDefault();

            var customerProvider = new CustomerProvider(_context);
            var updateResult = await customerProvider.UpdateCustomer(updatedCustomer);
            updateResult.Should().BeTrue();
        }

        [Fact]
        public async Task ShouldSuccessfullyDeleteCustomer()
        {
            var customerId = _context.Customers.FirstOrDefault()?.CustomerId;

            var customerProvider = new CustomerProvider(_context);
            var updateResult = await customerProvider.DeleteCustomer(customerId);
            updateResult.Should().BeTrue();
        }
    }
}
