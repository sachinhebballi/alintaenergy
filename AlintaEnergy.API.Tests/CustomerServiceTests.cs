﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AlintaEnergy.API.Models;
using AlintaEnergy.API.Providers;
using AlintaEnergy.API.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace AlintaEnergy.API.Tests
{
    public class CustomerServiceTests
    {
        private readonly Mock<ICustomerProvider> _customerProvider;

        public CustomerServiceTests()
        {
            _customerProvider = new Mock<ICustomerProvider>();
        }

        [Fact]
        public async Task ShouldSuccessfullyAddCustomers()
        {

            var customer = new CustomerModel
            {
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            var customerService = new CustomerService(_customerProvider.Object);
            await customerService.AddCustomer(customer);

            _customerProvider.Verify(_ => _.AddCustomer(customer), Times.Once);
        }

        [Fact]
        public async Task ShouldSuccessfullyReturnCustomersOnSearch()
        {

            var customerFirstName = "Joh";

            var customers = new List<CustomerModel>
            {
                new CustomerModel
                {
                    FirstName = "John",
                    LastName = "Doe",
                    DateOfBirth = "01/01/1970"
                }
            };

            _customerProvider.Setup(_ => _.SearchCustomers(customerFirstName)).Returns(Task.FromResult((IEnumerable<CustomerModel>)customers));

            var customerService = new CustomerService(_customerProvider.Object);
            var customersSearchResult = await customerService.SearchCustomers(customerFirstName);

            customersSearchResult.Should().NotBeNull();
            customersSearchResult.Should().BeEquivalentTo(customers);
        }

        [Fact]
        public async Task ShouldSuccessfullyUpdateCustomer()
        {

            var customer = new CustomerModel
            {
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            _customerProvider.Setup(_ => _.UpdateCustomer(customer)).Returns(Task.FromResult(true));

            var customerService = new CustomerService(_customerProvider.Object);
            var customerUpdateResult = await customerService.UpdateCustomer(customer);

            customerUpdateResult.Should().Be(true);
        }

        [Fact]
        public async Task ShouldSuccessfullyDeleteCustomer()
        {

            var customer = new CustomerModel
            {
                CustomerId = "17A8EFA4-4E27-4162-A941-8B4378045396",
                FirstName = "John",
                LastName = "Doe",
                DateOfBirth = "01/01/1970"
            };

            _customerProvider.Setup(_ => _.DeleteCustomer(customer.CustomerId)).Returns(Task.FromResult(true));

            var customerService = new CustomerService(_customerProvider.Object);
            var customerDeleteResult = await customerService.DeleteCustomer(customer.CustomerId);
            
            customerDeleteResult.Should().Be(true);
        }
    }
}
