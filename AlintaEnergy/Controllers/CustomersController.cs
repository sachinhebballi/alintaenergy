﻿using System.Collections.Generic;
using System.Net;
using AlintaEnergy.API.Models;
using AlintaEnergy.API.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AlintaEnergy.API.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        /// <summary>
        /// Initializes an instance of <see cref="CustomersController"/> class
        /// </summary>
        /// <param name="customerCustomerService">Customer service</param>
        public CustomersController(ICustomerService customerCustomerService)
        {
            _customerService = customerCustomerService;
        }

        /// <summary>
        /// Searches the list of customers with the query parameter
        /// </summary>
        /// <param name="q">Query Parameter</param>
        /// <returns>Returns a list of matching customers</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(List<CustomerModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SearchCustomers(string q)
        {
            if (string.IsNullOrWhiteSpace(q))
            {
                return BadRequest();
            }

            return Ok(await _customerService.SearchCustomers(q));
        }

        /// <summary>
        /// Adds a new customer
        /// </summary>
        /// <param name="customer">Customer model</param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddCustomer([FromBody]CustomerModel customer)
        {
            if (customer == null)
            {
                return BadRequest(new ApiResponse
                {
                    Code = Constants.PayloadEmpty.Code,
                    Message = Constants.PayloadEmpty.Message
                });
            }

            await _customerService.AddCustomer(customer);

            return StatusCode((int)HttpStatusCode.Created);
        }

        /// <summary>
        /// Updates the customer
        /// </summary>
        /// <param name="customer">Customer model</param>
        /// <returns>Returns the update status</returns>
        [HttpPut]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateCustomer([FromBody]CustomerModel customer)
        {
            if (customer == null)
            {
                return BadRequest(new ApiResponse
                {
                    Code = Constants.PayloadEmpty.Code,
                    Message = Constants.PayloadEmpty.Message
                });
            }

            await _customerService.UpdateCustomer(customer);

            return Ok();
        }

        /// <summary>
        /// Deletes the customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Returns the delete status</returns>
        [HttpDelete]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteCustomer(string customerId)
        {
            if (string.IsNullOrWhiteSpace(customerId))
            {
                return BadRequest(new ApiResponse
                {
                    Code = Constants.CustomerIdEmpty.Code,
                    Message = Constants.CustomerIdEmpty.Message
                });
            }

            await _customerService.DeleteCustomer(customerId);

            return Ok();
        }
    }
}