﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlintaEnergy.API.Models;

namespace AlintaEnergy.API.Providers
{
    public interface ICustomerProvider
    {
        /// <summary>
        /// Searches customers with the query
        /// </summary>
        /// <param name="q">Query parameter</param>
        /// <returns>Returns a list of customers matching the query parameter</returns>
        Task<IEnumerable<CustomerModel>> SearchCustomers(string q);

        /// <summary>
        /// Adds a new customer
        /// </summary>
        /// <param name="customer">Customer view model</param>
        /// <returns>Returns a list of customers matching the query parameter</returns>
        Task AddCustomer(CustomerModel customer);

        /// <summary>
        /// Updates the customer
        /// </summary>
        /// <param name="customer">Customer model</param>
        /// <returns>Returns the update status</returns>
        Task<bool> UpdateCustomer(CustomerModel customer);

        /// <summary>
        /// Deletes the customer
        /// </summary>
        /// <param name="customerId">Customer model</param>
        /// <returns>Returns the delete status</returns>
        Task<bool> DeleteCustomer(string customerId);
    }
}
