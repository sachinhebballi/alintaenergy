﻿using AlintaEnergy.API.Models;
using AlintaEnergy.API.Providers.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlintaEnergy.API.Providers
{
    public class CustomerProvider : ICustomerProvider
    {
        private readonly CustomersContext _context;

        public CustomerProvider(CustomersContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Searches customers with the query
        /// </summary>
        /// <param name="q">Query parameter</param>
        /// <returns>Returns a list of customers matching the query parameter</returns>
        public async Task<IEnumerable<CustomerModel>> SearchCustomers(string q)
        {
            q = q.ToLowerInvariant();
            var customers = await _context.Customers
                .Where(_ => _.FirstName.ToLowerInvariant().Contains(q) || _.LastName.ToLowerInvariant().Contains(q))
                .Select(customer => new CustomerModel
                {
                    CustomerId = customer.CustomerId,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    DateOfBirth = customer.DateOfBirth
                }).ToListAsync();

            return customers;
        }

        /// <summary>
        /// Adds a new customer
        /// </summary>
        /// <param name="customerModel">Customer view model</param>
        /// <returns>Returns a list of customers matching the query parameter</returns>
        public async Task AddCustomer(CustomerModel customerModel)
        {
            var customer = new Customer
            {
                CustomerId = Guid.NewGuid().ToString(),
                FirstName = customerModel.FirstName,
                LastName = customerModel.LastName,
                DateOfBirth = customerModel.DateOfBirth
            };

            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the customer
        /// </summary>
        /// <param name="customerModel">Customer model</param>
        /// <returns>Returns the update status</returns>
        public async Task<bool> UpdateCustomer(CustomerModel customerModel)
        {
            var customer = _context.Customers.FirstOrDefault(c => c.CustomerId == customerModel.CustomerId);
            if (customer == null) return false;

            customer.FirstName = customerModel.FirstName;
            customer.LastName = customerModel.LastName;
            customer.DateOfBirth = customerModel.DateOfBirth;

            return await _context.SaveChangesAsync() > 0;

        }

        /// <summary>
        /// Deletes the customer
        /// </summary>
        /// <param name="customerId">Customer model</param>
        /// <returns>Returns the delete status</returns>
        public async Task<bool> DeleteCustomer(string customerId)
        {
            var customer = _context.Customers.FirstOrDefault(c => c.CustomerId == customerId);

            if (customer == null) return false;
            _context.Customers.Remove(customer);

            return await _context.SaveChangesAsync() > 0;
        }
    }
}
