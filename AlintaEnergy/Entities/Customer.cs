﻿namespace AlintaEnergy.API.Models
{
    /// <summary>
    /// Customer class
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Gets or sets the customer id
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Gets or sets the last name
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Gets or sets the date of birth
        /// </summary>
        public string DateOfBirth { get; set; }
    }
}
