﻿using AlintaEnergy.API.Models;
using Microsoft.EntityFrameworkCore;

namespace AlintaEnergy.API.Providers.Entities
{
    public class CustomersContext : DbContext
    {
        public CustomersContext(DbContextOptions<CustomersContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}
