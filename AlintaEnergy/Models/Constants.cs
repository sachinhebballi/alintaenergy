﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlintaEnergy.API.Models
{
    public class Constants
    {
        public static readonly ApiResponse PayloadEmpty = new ApiResponse { Code = "1", Message = "Please provide customer details" };
        public static readonly ApiResponse CustomerIdEmpty = new ApiResponse { Code = "2", Message = "Please provide a value for Customer Id" };
    }
}
