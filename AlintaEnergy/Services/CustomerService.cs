﻿using AlintaEnergy.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using AlintaEnergy.API.Providers;

namespace AlintaEnergy.API.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerProvider _customerProvider;

        /// <summary>
        /// Initializes an instance of <see cref="CustomerService" /> class
        /// </summary>
        /// <param name="customerProvider"></param>
        public CustomerService(ICustomerProvider customerProvider)
        {
            _customerProvider = customerProvider;
        }

        /// <summary>
        /// Searches customers with the query
        /// </summary>
        /// <param name="q">Query parameter</param>
        /// <returns>Returns a list of customers matching the query parameter</returns>
        public async Task<IEnumerable<CustomerModel>> SearchCustomers(string q) => await _customerProvider.SearchCustomers(q);

        /// <summary>
        /// Adds a new customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Returns a list of customers matching the query parameter</returns>
        public async Task AddCustomer(CustomerModel customer) => await _customerProvider.AddCustomer(customer);

        /// <summary>
        /// Updates the customer
        /// </summary>
        /// <param name="customer">Customer model</param>
        /// <returns>Returns the update status</returns>
        public async Task<bool> UpdateCustomer(CustomerModel customer) => await _customerProvider.UpdateCustomer(customer);

        /// <summary>
        /// Deletes the customer
        /// </summary>
        /// <param name="customerId">Customer model</param>
        /// <returns>Returns the delete status</returns>
        public async Task<bool> DeleteCustomer(string customerId) => await _customerProvider.DeleteCustomer(customerId);
    }
}
